<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin Login</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" />
</head>
<body>
    @include('Partial.messages')
<div class="container margin-top-60">
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 offset-xs-4 offset-sm-4 offset-md-4">
            <div class="card card-default" style="background-color: #eee">
                <div class="card-header" style="background-color: green;color: #fff">Admin logout</div>
                  <form action="{{ route('admin.logout') }}" method="POST">
                        {{ csrf_field() }}
                        <button type="submit" name="submit">Logout</button>
                  </form>
            
            </div>
        </div>
    </div>
</div>
    <script src="{{ asset('js/slim.min.js') }}"></script>
    <script src="{{ asset('js/propper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
</body>
</html>

