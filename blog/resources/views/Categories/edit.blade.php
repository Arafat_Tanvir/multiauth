@extends('Layouts.master')
@section('content')
<div class="container margin-top-20">
	<div class="row">
		<div class="col-xs-8 col-sm-8 col-md-8">
			@if((Session::get('message')))
			<div class="alert alert-success alert-dismissable">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					{{ Session::get('message')}}
				</div>
			</div>
			@endif
			<div class="card-header" style="text-align: center; background-color: #7a7c7c; color: #ffffff;margin-bottom: 20px">
				<strong>categories Create Form</strong>
			</div>
			<form role="form" class="form-horizontal" action="{{route('categories.update',$categories->id)}}" method="post" enctype="multipart/form-data">
				{{ csrf_field()}}
				<div class="form-group">
					<label>Name</label>
					<input type="text" class="form-control" name="name" value="{{ $categories->name }}" required>
					<p class="help-block">{{ ($errors->has('name')) ? $errors->first('name') : ''}}</p>
				</div>

				
				<div class="form-group">
					<label>Description</label>
					<textarea name="description" cols="4" rows="5" class="form-control"required>{{ $categories->description}}</textarea>
					<p class="help-block">{{ ($errors->has('description')) ? $errors->first('description') : ''}}</p>
				</div>

				<div class="form-group">
                    <label>Parent Category Name</label>
                    <select name="parent_id" class="form-control" value="{{old('parent_id')}}">
                        <option value="0" disabled="true" selected="true">===Select Parent Category for this Category===</option>
                        @foreach($main_categories as $category)
                        <option value="{{$category->id}}" {{ $category->id ==$categories->parent_id ? 'selected' : ''}}>{{$category->name}}</option>
                        @endforeach
                    </select>
                    <p class="help-block">{{ ($errors->has('parent_id')) ? $errors->first('parent_id') : ''}}</p>
                </div>

				<div class="form-group">
					<label>Image</label>
					<div class="row">
						<div class="col-sm-4" style="margin-top: 5px">
							<input type="file" name="images" value="{{ old('images')}}">
						</div>
						<p class="help-block">{{ ($errors->has('images')) ? $errors->first('images') : ''}}</p>
					</div>
				</div>
				<input type="submit" style="font-size: 20px;" class="btn btn-primary" name="submit" value="update categories">
			</form>
		</div>
		<div class="col-sm-4">
				<div id="demo" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ul class="carousel-indicators">
						<li data-target="#demo" data-slide-to="0" class="active"></li>
						<li data-target="#demo" data-slide-to="1"></li>
						<li data-target="#demo" data-slide-to="2"></li>
					</ul>
					<!-- The slideshow -->
					<div class="carousel-inner">
						<div class="carousel-item active">
								<img src="{{asset('images/categories/'.$categories->images)}}" alt="Los Angeles">
						</div>
						<div class="carousel-item">
								<img src="{{asset('images/categories/'.$categories->images)}}" alt="Los Angeles">
						</div>
						<div class="carousel-item">
								<img src="{{asset('images/categories/'.$categories->images)}}" alt="Los Angeles">
						</div>
						
					</div>
					<!-- Left and right controls -->
					<a class="carousel-control-prev" href="#demo" data-slide="prev">
						<span class="carousel-control-prev-icon"></span>
					</a>
					<a class="carousel-control-next" href="#demo" data-slide="next">
						<span class="carousel-control-next-icon"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
	@endsection