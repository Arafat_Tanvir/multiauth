@extends('Layouts.master')
@section('content')
<div class="container margin-top-20">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			@if((Session::get('succss')))
			<div class="alert alert-success alert-dismissable">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					{{ Session::get('message')}}
				</div>
			</div>
			@endif
			<div class="card-header" style="text-align: center; background-color: #7a7c7c; color: #ffffff;margin-bottom: 20px">
				<strong>Product Create Form</strong>
			</div>
			<table id="schedule" class="table table-bordered">
				<thead>
					<tr>
						<th rowspan="2">SL</th>
						<th rowspan="2">Name</th>
						<th rowspan="2">Image</th>
						<th rowspan="2">Parent ID</th>
						<th rowspan="2">Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<div style="display: none;">{{$a=1}}</div>
						@foreach($categories as $category)
						<td>{{ $a++ }}</td>
						<td>{{ $category->name }}</td>
						<td style="text-align: center;">
							<img src="{{asset('images/categories/'.$category->images)}}" alt="" height="120px" width="220">
						</td>
						<td style="color: red">
							@if($category->parent_id==null)
							Primary Category
							@else
							{{ $category->parent->name }}
							@endif
						</td>
						<td>
							<a href="{{route('categories.edit', $category->id)}}" class="btn btn-warning btn-sm">Edit</a>
							<a href="#DeleteModal{{ $category->id}}" data-toggle="modal" class="btn btn-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('categories.delete', $category->id)}}" method="POST">
													{{csrf_field()}}
												<button type="submit" class="btn btn-primary btn-sm">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection