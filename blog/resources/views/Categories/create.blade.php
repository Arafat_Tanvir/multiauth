@extends('Layouts.master')
@section('content')
<div class="container margin-top-20">
    <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8 offset-xs-2 offset-sm-2 offset-md-2">
            @if((Session::get('message')))
            <div class="alert alert-success alert-dismissable">
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message')}}
                </div>
            </div>
            @endif
            <div class="card-header" style="text-align: center; background-color: #7a7c7c; color: #ffffff;margin-bottom: 20px">
                <strong>Product Create Form</strong>
            </div>
            <form role="form" class="form-horizontal" action="{{route('categories.store')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field()}}
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" value="{{ old('name')}}" required>
                    <p class="help-block">{{ ($errors->has('name')) ? $errors->first('name') : ''}}</p>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea name="description" cols="4" rows="5" value="{{old('description')}}" class="form-control"required>{{ old('description')}}</textarea>
                    <p class="help-block">{{ ($errors->has('description')) ? $errors->first('description') : ''}}</p>
                </div>

                <div class="form-group">
                    <label>Parent Category Name</label>
                    <select name="parent_id" class="form-control" value="{{old('parent_id')}}">
                        <option value="0" disabled="true" selected="true">===Select Parent Category for this Category===</option>
                        @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                    <p class="help-block">{{ ($errors->has('parent_id')) ? $errors->first('parent_id') : ''}}</p>
                </div>

                <div class="form-group">
                    <label>Image</label>
                    <div class="row">
                        <div class="col-sm-4" style="margin-top: 5px">
                            <input type="file" name="images" value="{{ old('images')}}">
                        </div>
                        <p class="help-block">{{ ($errors->has('images')) ? $errors->first('images') : ''}}</p>
                    </div>
                </div>
                <input type="submit" style="font-size: 20px;" class="btn btn-primary" name="submit" value="Add Product">
            </form>
        </div>
    </div>
</div>
@endsection