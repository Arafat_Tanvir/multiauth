@extends('Layouts.master')

@section('title')
{{$products->title }} || Laravel ecommarce Site
@endsection

@section('content')
<div class="container">
	<div class="row margin-top-20">
		<div class="col-sm-4">
			<div id="demo" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					@php
					$i=1
					@endphp
					@foreach($products->images as $image)
					<div class="carousel-item {{$i==1 ? 'active' : ''}}">
						<img src="{{asset('images/products/'.$image->images)}}" alt="" height=300">
					</div>
					@php
					$i++
					@endphp
					@endforeach
				</div>
				<!-- Left and right controls -->
				<a class="carousel-control-prev" href="#demo" data-slide="prev">
					<span class="carousel-control-prev-icon"></span>
				</a>
				<a class="carousel-control-next" href="#demo" data-slide="next">
					<span class="carousel-control-next-icon"></span>
				</a>
			</div>
		</div>
		<div class="col-sm-8">
			<div class="widget">
				<div class="card">
					<h4 style="text-align: center;">{{$products->title}}</h4>
				</div>
			</div>
			<div class="widget">
				<div class="card">
					<div class="row">
						<div class="col-sm-2">
							<span>Brand Name</span></br>
							<span>Categories</span></br>
							<span>Price</span></br>
						</div>
						<div class="col-sm-7">
							<span>-> {{$products->brands->name}}</span></br>
							<span>-> {{$products->categories->name}}</span></br>
							<span>-> {{$products->price}} Taka only</span>
						</div>
						<div class="col-sm-3">
							<span>{{$products->status}}</span></br>
							<span class="badge badge-primary">{{$products->quantity < 1 ? 'No Item is Available' :$products->quantity.' Item is stock'}}</span></br>
							<span>{{$products->slug}}</span></br>
							<span>{{$products->offer_price}}</span>
						</div>
					</div>
					<h4 style="text-align: center; color: #fff;background-color: pink">Description</h4>
					<p>{{$products->description}}</p>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection