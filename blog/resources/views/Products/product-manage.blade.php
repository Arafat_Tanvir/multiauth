@extends('Layouts.master')

@section('content')
<div class="container margin-top-20">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			@if((Session::get('message')))
			<div class="alert alert-success alert-dismissable">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					{{ Session::get('message')}}
				</div>
			</div>
			@endif
			<div class="card-header" style="text-align: center; background-color: #7a7c7c; color: #ffffff;margin-bottom: 20px">
				<strong>Product Create Form</strong>
			</div>
			<table id="schedule" class="table table-bordered">
				<thead>
					<tr>
						<th rowspan="2">SL</th>
						<th rowspan="2">title</th>
						<th rowspan="2">Image</th>
						<th rowspan="2">quantity</th>
						<th rowspan="2">price</th>
						<th rowspan="2">Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<div style="display: none;">{{$a=1}}</div>
						@foreach($products as $product)
						<td>{{ $a++ }}</td>
						<td>{{ $product->title }}</td>
						<td>
							<div id="demo" class="carousel slide" data-ride="carousel">
								<!-- The slideshow -->
								<div class="carousel-inner">
									<div class="carousel-inner">
										@php
										$i=1
										@endphp
										@foreach($product->images as $image)
										<div style="text-align: center;" class="carousel-item {{$i==1 ? 'active' : ''}}">
											<img class="card-img-top-image" src="{{asset('images/products/'.$image->images)}}" alt="" height=300">
										</div>
										@php
										$i++
										@endphp
										@endforeach
									</div>
								</div>
								<!-- Left and right controls -->
								<a class="carousel-control-prev" href="#demo" data-slide="prev">
									<span class="carousel-control-prev-icon"></span>
								</a>
								<a class="carousel-control-next" href="#demo" data-slide="next">
									<span class="carousel-control-next-icon"></span>
								</a>
							</div>
						</td>
						<td>{{ $product->quantity }}</td>
						<td>{{ $product->price }}</td>
						<td>
							<a href="{{route('products.edit', $product->id)}}" class="btn btn-warning btn-sm">Edit</a>
							<a href="#DeleteModal{{ $product->id}}" data-toggle="modal" class="btn btn-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('products.destroy', $product->id)}}" method="POST">
													{{csrf_field()}}
													{{method_field('DELETE')}}
												<button type="submit" class="btn btn-primary btn-sm">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection