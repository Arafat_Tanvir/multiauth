@extends('Layouts.master')

@section('title')
{{$product->title }} || Laravel ecommarce Site
@endsection

@section('content')
<div class="container margin-top-20">
	<div class="row">
		<div class="col-xs-8 col-sm-8 col-md-8">
			@if((Session::get('message')))
			<div class="alert alert-success alert-dismissable">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					{{ Session::get('message')}}
				</div>
			</div>
			@endif
			<div class="card-header" style="text-align: center; background-color: #7a7c7c; color: #ffffff;margin-bottom: 20px">
				<strong>Product Create Form</strong>
			</div>
			<form role="form" class="form-horizontal" action="{{route('products.store')}}" method="post" enctype="multipart/form-data">
				{{ csrf_field()}}
				<div class="form-group">
					<label>title</label>
					<input type="text" class="form-control" name="title" value="{{ $product->title }}" required>
					<p class="help-block">{{ ($errors->has('title')) ? $errors->first('title') : ''}}</p>
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea name="description" cols="4" rows="5" class="form-control"required>{{ $product->description}}</textarea>
					<p class="help-block">{{ ($errors->has('description')) ? $errors->first('description') : ''}}</p>
				</div>

				<div class="form-group">
                    <label>Choose Category</label>
                    <select name="category_id" class="form-control" value="{{old('category_id')}}" required>
                        <option value="0" disabled="true" selected="true">===Select Category===</option>
                        @foreach(App\Category::orderBy('name','desc')->where('parent_id',NULL)->get(); as $parent)
                        <option value="{{$parent->id}}" {{ $parent->id ==$product->category_id ? 'selected' : ''}}>{{$parent->name}}</option>
                        @foreach(App\Category::orderBy('name','desc')->where('parent_id',$parent->id)->get(); as $child)
                            <option value="{{$child->id}}" {{ $child->id ==$product->category_id ? 'selected' : ''}}>--->{{$child->name}}</option>
                        @endforeach
                        @endforeach
                    </select>
                    <p class="help-block">{{ ($errors->has('category_id')) ? $errors->first('category_id') : ''}}</p>
                </div>
                <div class="form-group">
                    <label>Choose Brand</label>
                    <select name="brand_id" class="form-control" value="{{old('brand_id')}}" required>
                        <option value="0" disabled="true" selected="true">===Select Brand===</option>
                        @foreach($brands as $brand)
                        <option value="{{$brand->id}}" {{ $brand->id ==$product->brand_id ? 'selected' : ''}}>{{$brand->name}}</option>
                        @endforeach
                    </select>
                    <p class="help-block">{{ ($errors->has('brand_id')) ? $errors->first('brand_id') : ''}}</p>
                </div>

				<div class="form-group">
					<label>Quantity</label>
					<input type="number" name="quantity" class="form-control" value="{{ $product->quantity }}" required>
					<p class="help-block">{{ ($errors->has('quantity')) ? $errors->first('quantity') : ''}}</p>
				</div>
				<div class="form-group">
					<label>Price</label>
					<input type="number" name="price" class="form-control" value="{{ $product->price}}" required>
					<p class="help-block">{{ ($errors->has('price')) ? $errors->first('price') : ''}}</p>
				</div>
				<div class="form-group">
					<label>Image</label>
					<div class="row">
						<div class="col-sm-4" style="margin-top: 5px">
							<input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
						</div>
						<div class="col-sm-4" style="margin-top: 5px">
							<input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
						</div>
						<div class="col-sm-4" style="margin-top: 5px">
							<input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
						</div>
						<div class="col-sm-4" style="margin-top: 5px">
							<input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
						</div>
						<div class="col-sm-4" style="margin-top: 5px">
							<input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
						</div>
						<div class="col-sm-4" style="margin-top: 5px">
							<input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
						</div>
						<p class="help-block">{{ ($errors->has('images')) ? $errors->first('images') : ''}}</p>
					</div>
				</div>
				<input type="submit" style="font-size: 20px;" class="btn btn-primary" name="submit" value="Add Product">
			</form>
		</div>
		<div class="col-sm-4">
				<div id="demo" class="carousel slide" data-ride="carousel">
					<!-- The slideshow -->
					<div class="carousel-inner">
						<div class="carousel-inner">
					@php
					$i=1
					@endphp
					@foreach($product->images as $image)
					<div class="carousel-item {{$i==1 ? 'active' : ''}}">
						<img src="{{asset('images/products/'.$image->images)}}" alt="" height=300">
					</div>
					@php
					$i++
					@endphp
					@endforeach
				</div>
					</div>
					<!-- Left and right controls -->
					<a class="carousel-control-prev" href="#demo" data-slide="prev">
						<span class="carousel-control-prev-icon"></span>
					</a>
					<a class="carousel-control-next" href="#demo" data-slide="next">
						<span class="carousel-control-next-icon"></span>
					</a>
				</div>
		</div>
		</div>
	</div>
	@endsection