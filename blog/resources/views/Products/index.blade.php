@extends('Layouts.master')

@section('content')
<section class="margin-top-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				@include('Partial.products-sidebar')
			</div>
			<div class="col-sm-8">
				@include('Partial.all-products')
			</div>
		</div>
	</div>
	</section><!--service area end -->
	@endsection