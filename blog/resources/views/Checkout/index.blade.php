@extends('Layouts.master')
@section('content')
<div class="container margin-top-20">
	<div class="row justify-content-start">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
					<strong>Confirm Item</strong>
				</div>
				<div class="card-body" style="background: #eee">
					<div class="row">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-7 border-right">
									@foreach(App\Card::total_Cards() as $card)
									<p>
										Product Name : <strong>{{ $card->product->title}}</strong>
									</p>
									<p>
										Price : <strong>{{ $card->product->price}}</strong>==>
										{{$card->product_quantity}}
									</p>
									@endforeach
									<a href="{{route('cards.index')}}"> Change Card Item</a>
								</div>
								<div class="col-sm-5">
									@php
									$total_price=0;
									@endphp
									@foreach(App\Card::total_Cards() as $card)
									@php
									$total_price+=$card->product->price * $card->product_quantity
									@endphp
									@endforeach

									<p> Total Price : <strong>{{ $total_price }}</strong></p>
									<p> Total Price  with Shipping cost : <strong>{{ $total_price + App\Setting::first()->shipping_cost }}</strong></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row justify-content-center margin-top-20">
		<div class="col-sm-6">
			<div class="card">
				<div class="card-header">
					<strong>Confirm Item</strong>
				</div>
				<div class="card-body" style="background: #eee">
					<form class="form-horizontal" method="POST" action="{{ route('orders.store')}}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
							
							<div class="col-md-12">
								<label for="last_name" class="control-label">Reciver last_name</label>
								<div class="input-group">
									<div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
									<input id="last_name" type="text" class="form-control" name="last_name" value="{{ Auth::check() ? Auth::user()->last_name : '' }}" placeholder="Enter Your last_name" autofocus>
									@if ($errors->has('last_name'))
									<span class="help-block">
										<strong>{{ $errors->first('last_name') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							
							<div class="col-md-12">
								<label for="email" class="control-label">Reciver Email</label>
								<div class="input-group">
									<div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
									<input id="email" type="email" class="form-control" name="email" value="{{ Auth::check() ? Auth::user()->email :''}}" placeholder="Enter Your email" autofocus>
									@if ($errors->has('email'))
									<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>

						<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="message" class="control-label">Message</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-comment"></span></div>
                                    <textarea name="message" id="message" class="form-control" cols="30" rows="5" value="{{ old('message') }}"placeholder="Enter Your message"  autofocus></textarea>
                                    @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('payment_method_id') ? ' has-error' : '' }}">
							
							<div class="col-md-12">
								<label for="payment_method_id" class="control-label">Payment Method</label>
								<div class="input-group">
									<select name="payment_method_id" class="form-control" value="{{old('payment_method_id')}}" id="payments">
										<option value="0" disabled="true" selected="true">===Select Payment Method===</option>
										@foreach($payments as $payment)
										<option value="{{$payment->short_name}}">{{$payment->name}}</option>
										@endforeach
									</select>
								</div>
								@if ($errors->has('payment_method_id'))
									<span class="help-block badge badge-danger" >
										<strong>{{ $errors->first('payment_method_id') }}</strong>
									</span>
								@endif
							</div>
							@foreach($payments as $payment)
							<div class="margin-top-20">
							@if($payment->short_name=="Cash")
							<div id="payment_{{ $payment->short_name}}" class="hidden alert alert-success">
								<div class="cash">
									<div class="badge badge-success">
										<img src="{{asset('images/payments/'.$payment->image)}}" alt="Rocket Image" class="img-fluid img-thumbnail">
									</div>
									<hr>
									<h4 class="badge badge-warning"> {{$payment->name}} Payment </h4><br>
									<h5 class="alert alert-info">For Cash in there is noting necssary.Just Finish Your Order</h5><br>
									<div class="alert alert-danger">
											<small>You Will get Your Products in Two or Three bussiness Days </small>
									</div>
								</div>
							</div>
							@else
							<div id="payment_{{ $payment->short_name}}" class="hidden alert alert-success">
								<div class="bikash">
									<div class="badge badge-success">
										<img src="{{asset('images/payments/'.$payment->image)}}" alt="Rocket Image" class=" img-fluid img-thumbnail">
									</div>
									<hr>
									<h4 class="badge badge-warning"> {{$payment->name}} Payment </h4>
									<p>
										<strong>{{$payment->name}} No: {{ $payment->no}}</strong><br>
										<strong class="badge badge-info"> Account Type: {{ $payment->type}}</strong>
										<div class="alert alert-danger">
											<p>
											Please send the Above money to this <span badge badge-success>{{$payment->name}}</span> money and write your transaction code bellow  there
											</p>
										</div>
									</p>
								</div>
							</div>
							@endif
							</div>
							@endforeach
							<input type="text" id="transaction_id" placeholder="Please inter Transaction ID" name="transaction_id" class="form-control hidden">
						</div>
						<button type="submit" class="btn btn-primary">
                        Order
                        </button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$("#payments").change(function() {
		//alert('dsfaskf');
		$payment_method=$("#payments").val();
		if ($payment_method=="Cash") {
			$("#payment_Cash").removeClass('hidden');
			$("#payment_Rocket").addClass('hidden');
			$("#payment_Bikash").addClass('hidden');
		}else if($payment_method=="Bikash"){
			$("#payment_Bikash").removeClass('hidden');
			$("#transaction_id").removeClass('hidden');
			$("#payment_Cash").addClass('hidden');
			$("#payment_Rocket").addClass('hidden');
		}else if($payment_method=="Rocket"){
			$("#payment_Rocket").removeClass('hidden');
			$("#transaction_id").removeClass('hidden');
			$("#payment_Cash").addClass('hidden');
			$("#payment_Bikash").addClass('hidden');
		}
	});
</script>
@endsection