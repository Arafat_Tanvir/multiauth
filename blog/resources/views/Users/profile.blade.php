@extends('Users.master')
@section('sub-content')
<div class="card">
          <div class="card-header">
            <strong class="card-title mb-3">Profile</strong>
          </div>
          <div class="row">
            <div class="col-sm-4 mt-2">
              <img src="{{ App\Helper\ImageHelper::getUserImage(Auth::user()->id) }}" alt="" class="img-thumbnail" style="height: 220px;width: 220px;">
            </div>
            <div class="col-sm-3">
              <h4><span>First Name</span></h4><hr>
              <h4><span>Last Name</span></h4><hr>
              <h4><span>Eamil</span></h4><hr>
              <h4><span>Phone Number</span></h4><hr>
              <h4><span>Shipping Address</span></h4><hr>
              <h4><span>Division</span></h4><hr>
              <h4><span>District</span></h4><hr>
              <h4><span>Upazila</span></h4><hr>
              <h4><span>Union</span></h4><hr>
              <h4><span>Street Address</span></h4><hr>
            </div>
            <div class="col-sm-5">
              <h4><span>$user->first_name}}</span></h4>
              <h4><span>$user->last_name}}</span></h4>
              <h4><span>$user->email}}</span></h4>
              <h4><span>$user->phone}}</span></h4>
              <h4><span>$user->shipping_address}}</span></h4>
              <h4><span>$user->divition_id}}</span></h4>
              <h4><span>$user->district_id}}</span></h4>
              <h4><span>$user->upazila_id}}</span></h4>
              <h4><span>$user->union_id}}</span></h4>
              <h4><span>{{$user->street_address}}</span></h4>
            </div>
          </div>
        </div>
@endsection