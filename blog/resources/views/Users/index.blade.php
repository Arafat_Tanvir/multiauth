@extends('Users.master')
@section('sub-content')
<div class="container">
	
			<h2>Welcome {{ $user->first_name.' '.$user->last_name}}</h2>
			<p>You can change your profile and every informations here.....</p>
			<div class="row">
				<div class="col-sm-4">
					<div class="card mt-2 pointer" onclick="location.href='{{ route('dashboard')}}'">
						profile
					</div>
					<div class="card mt-2 pointer" onclick="location.href='{{ route('dashboard.profile')}}'">
						Update
					</div>
				</div>
			</div>
</div>

@endsection