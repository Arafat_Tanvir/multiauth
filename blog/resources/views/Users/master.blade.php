@extends('Layouts.master')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-4">
					<div class="card">
					<img src="{{ App\Helper\ImageHelper::getUserImage(Auth::user()->id) }}" alt="" class="img-thumbnail rounded-circle" style="height: 100px;width: 100px;">
						<ul class="list-group">
						    <a class="list-group-item {{ route('dashboard')? 'active' : ''}}" href="{{ route('dashboard')}}">Dashboard</a>
						    <a class="list-group-item " href="">Logout</a>
					  </ul>
				</div>
				</div>
				<div class="col-sm-8">
					<div class="card">

					@yield('sub-content')

				</div>
               </div>
			</div>
		</div>
	</div>
</div>
@endsection