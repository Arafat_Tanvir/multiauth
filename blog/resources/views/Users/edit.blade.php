@extends('Layouts.master')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8 offset-xs-2 offset-sm-2 offset-md-2">
            <div class="card card-default" style="background-color: #eee">
                <div class="card-header" style="background-color: green;color: #fff">Registration</div>
                    <form class="form-horizontal mt-2" method="POST" action="{{ route('dashboard.update') }}" enctype="multipart/form-data" >
                        {{ csrf_field() }}

                            <div class="row">
                              <div class="col">
                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                  <label for="first_name" class="col-md-12 control-label">First Name</label>
                                  <div class="col-md-12">
                                    <input id="first_name" type="text" name="first_name" class="form-control" first_name="first_name" value="{{ $user->first_name }}">
                                    @if ($errors->has('first_name'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                  </div>
                                </div>
                              </div>
                              <div class="col">
                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-12 control-label">Last Name</label>

                            <div class="col-md-12">
                                <input id="last_name" type="text" name="last_name" class="form-control" last_name="last_name" value="{{ $user->last_name }}">

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                              </div>
                              <div class="w-100"></div>
                              <div class="col">
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-12 control-label">Phone</label>

                            <div class="col-md-12">
                                <input id="phone" type="number" name="phone" class="form-control" phone="phone" value="{{ $user->phone }}">

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                              </div>
                              <div class="col">
                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                  <label for="username" class="col-md-12 control-label">Username</label>
                                  <div class="col-md-12">
                                    <input id="username" type="text" name="username" class="form-control" id="username" value="{{ $user->username }}">
                                    @if ($errors->has('username'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                  </div>
                                </div>
                              </div>
                        </div>

                        <div class="form-group{{ $errors->has('images') ? ' has-error' : '' }}">
                            <label for="images" class="col-md-12 control-label">Images</label>

                            <div class="col-md-12">
                                <input id="images" type="file" name="images" class="form-control" value="{{ old('images') }}">

                                @if ($errors->has('images'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('images') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-12 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" >

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('shipping_address') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="shipping_address" class="control-label">Shipping Address</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-comment"></span></div>
                                    <textarea name="shipping_address" id="shipping_address" class="form-control" cols="30" rows="5" value="{{ old('shipping_address') }}"placeholder="Enter Your shipping_address" >{{ $user->shipping_address }}</textarea>
                                    @if ($errors->has('shipping_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shipping_address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group{{ $errors->has('division_id') ? ' has-error' : '' }}">
                              <label for="division_id" class="col-md-12 control-label">Division</label>
                              <div class="col-md-12">
                                  <select name="division_id" id="division_id" class="form-control">
                                      <option value="0" disabled="true" selected="true">===Choose Divisio Name==</option>
                                      @foreach(App\Division::orderBy('name','desc')->get(); as $division)
                                      <option value="{{$division->id}}" {{ $division->id ==$user->division_id ? 'selected' : ''}}>{{$division->name}}</option>
                                      @endforeach
                                    </select>
                                  @if ($errors->has('division_id'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('division_id') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('district_id') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="district_id" class="control-label">District</label>
                                <div class="input-group">
                                    <select name="district_id" id="district_id" class="form-control">
                                      <option value="0" disabled="true" selected="true">===Choose District name==</option>
                                    </select>
                                    @if ($errors->has('district_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('district_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                          </div>
                          <div class="col-sm-6">
                             <div class="form-group{{ $errors->has('upazila_id') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="upazila_id" class="control-label">Upazila</label>
                                <div class="input-group">
                                    <select name="upazila_id" id="upazila_id" class="form-control">
                                      <option value="0" disabled="true" selected="true">===Choose Upazila Name==</option>
                                    </select>
                                    @if ($errors->has('upazila_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('upazila_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('union_id') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="union_id" class="control-label">Union</label>
                                <div class="input-group">
                                    <select name="union_id" id="union_id" class="form-control">
                                      <option value="0" disabled="true" selected="true">===Choose Union Name==</option>
                                    </select>
                                    @if ($errors->has('union_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('union_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                          </div>
                        </div>

                        <div class="form-group{{ $errors->has('street_address') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="street_address" class="control-label">State Address</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-comment"></span></div>
                                    <textarea name="street_address" id="street_address" class="form-control" cols="30" rows="5" value="{{ old('street_address') }}"placeholder="Enter Your street_address ">{{ $user->street_address }}</textarea>
                                    @if ($errors->has('street_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('street_address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){
    $('select[name="division_id"]').on('change',function(){
      var division_id=$(this).val();
      //alert(division_id);
      if(division_id){
        $.ajax({
          url:'{{ url('')}}/divisions/ajax/'+division_id,
          type:"GET",
          dataType:"json",
          success:function(data){
            $('select[name="district_id"]').empty();
            $.each(data,function(key,value){
            $('select[name="district_id"]').append('<option value="'+key+'">'+value+'</option>')
            });
          }
        });
      }else{
        $('select[name="district_id"]').empty();
        }
    });
    $('select[name="district_id"]').on('change',function(){
      var district_id=$(this).val();
      //alert(district_id);
      console.log(district_id);
      if(district_id){
        $.ajax({
          url:'{{ url('')}}/districts/ajax/'+district_id,
          type:"GET",
          dataType:"json",
          success:function(data){
          $('select[name="upazila_id"]').empty();
            $.each(data,function(key,value){
            $('select[name="upazila_id"]').append('<option value="'+key+'">'+value+'</option>')
            });
          }
        });
      }else{
        $('select[name="upazila_id"]').empty();
      }
  });
    $('select[name="upazila_id"]').on('change',function(){
      var upazila_id=$(this).val();
      //alert(upazila_id);
      console.log(upazila_id);
      if(upazila_id){
        $.ajax({
          url:'{{ url('')}}/upazilas/ajax/'+upazila_id,
          type:"GET",
          dataType:"json",
          success:function(data){
          $('select[name="union_id"]').empty();
            $.each(data,function(key,value){
            $('select[name="union_id"]').append('<option value="'+key+'">'+value+'</option>')
            });
          }
        });
      }else{
        $('select[name="union_id"]').empty();
      }
  });
});
</script>

@endsection