@extends('Layouts.master')

@section('content')
<div class="container mt-3">
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 offset-xs-4 offset-sm-4 offset-md-4">
            <div class="card card-default" style="background-color: #eee">
                <div class="card-header" style="background-color: green;color: #fff">User Login</div>
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label style="margin-left: 16px">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                        </div>
                        <div class="form-group">
                            <label style="margin-left: 16px">
                                <button type="submit" class="btn btn-primary">Login</button> <span><a class="btn btn-link" href="{{ route('password.request') }}">Forgot Your Password?</a></span>
                            </label>
                        </div>
                    </form>
            
            </div>
        </div>
    </div>
</div>
@endsection
