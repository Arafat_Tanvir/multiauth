<div class="widget">
	<div class="product-details" style="color: #fff;background-color: #ccaacc;text-align: center; size: 20px">
		<h4>Product List</h4>
	</div>
	<div class="row">
		@foreach($products as $product)
		<div class="col-sm-4">
			<div class="card margin-top-20">
				@php
				$i=1
				@endphp
				@foreach($product->images as $image)
				@if($i>0)
				<a href="{{route('products.show',$product->slug)}}">
					<img class="card-img-top rounded-circle " src="{{asset('images/products/'.$image->images)}}" alt="Card image">
				</a>
				@endif
				@php
				$i--
				@endphp
				
				@endforeach
				<div class="card-body">
					<div class="products-details">
						<a href="{{route('products.show',$product->slug)}}"><h5 class="card-title">{{$product->title}}</h5></a>
					</div>
					<div class="products-price">
						<p class="card-text">Price: {{$product->price}}</p>
						@include('Cards/card-button')
					</div>
					
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>