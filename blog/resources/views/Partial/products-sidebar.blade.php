<ul class="list-group">
	@foreach(App\Category::orderBy('name','desc')->where('parent_id',NULL)->get(); as $parent)
		<a href="#main-{{$parent->id}}" data-toggle="collapse" class="list-group-item list-group-item-success btn-outline-success"><span><img src="{{asset('images/categories/'.$parent->images)}}" height="50px" width="50px" class="rounded-circle" alt="Los Angeles">{{$parent->name}}</span></a>

		<div class="child-rows collapse 
		@if(Route::is('categories-show'))
			@if(App\Category::ParentOrNotCategory($parent->id,$categories->id))
				   show
			@endif
		@endif
		" id="main-{{$parent->id}}">
			<div class="child-rows">
				@foreach(App\Category::orderBy('name','desc')->where('parent_id',$parent->id)->get(); as $child)
					<a href="{{route('categories-show',$child->id)}}" class="list-group-item list-group-item-wanning 
						@if(Route::is('categories-show'))
						@if($child->id==$categories->id)
						   active
						@endif
					@endif">

					<img src="{{asset('images/categories/'.$child->images)}}" height="40px" width="40px" class="rounded-circle" alt="Los Angeles">{{$child->name}}</a>
				@endforeach
			</div>
		</div>
	@endforeach
</ul>