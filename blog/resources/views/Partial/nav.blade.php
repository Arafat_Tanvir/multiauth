				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<div class="container">
						<a class="navbar-brand" href="{{ url('/') }}">
							{{ config('app.name', 'E-Commarce') }}
						</a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav mr-auto">
								<li class="nav-item active">
									<a class="nav-link" href="{{route('index')}}">Home</a>
								</li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Products
									</a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<a class="dropdown-item" href="{{route('products.index')}}">Products</a>
										<a class="dropdown-item" href="{{route('product-manage')}}">Product Manage</a>
										<a class="dropdown-item" href="{{route('products.create')}}">Product Create</a>
										<!-- <div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Something else here</a> -->
									</div>
								</li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Categories
									</a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<a class="dropdown-item" href="{{route('categories.index')}}">Categories Manage</a>
										<a class="dropdown-item" href="{{route('categories.create')}}">Categories Create</a>
										<!-- <div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Something else here</a> -->
									</div>
								</li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Drands
									</a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<a class="dropdown-item" href="{{route('brands.index')}}">brands Manage</a>
										<a class="dropdown-item" href="{{route('brands.create')}}">brands Create</a>
										<!-- <div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Something else here</a> -->
									</div>
								</li>
								<li class="nav-item">
									<form class="form-inline" action="{{route('product-search')}}" method="get">
										<input class="form-control" name="search" type="search" placeholder="Search" aria-label="Search">
										<button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
									</form>
								</li>
								<li  class="nav-item" style="margin-top: 4px;margin-left: 12px">
									<button class="btn btn-danger btn-sm">
										<span class="mt-1">Card</span>
										<span class="badge badge-warnning">
											<a href="{{route('cards.index')}}">{{ App\Card::total_Items() }}</a>
										</span>
									</button>
								</li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								
								<!-- Authentication Links -->
								@if (Auth::guest())
								<li><a class="btn btn-sm" href="{{ route('login') }}">Login</a></li>
								<li><a class="btn btn-sm" href="{{ route('register') }}">Register</a></li>
								@else
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
										<img src="{{ App\Helper\ImageHelper::getUserImage(Auth::user()->id) }}" alt="" class="img-thumbnail rounded-circle" style="height: 40px;width: 40px">
									</a>
									<ul class="dropdown-menu" role="menu">
										<li>
											<a href="{{ route('dashboard') }}" class="badge badge-success">Dashboard</a>
										</li>
										<li>
											<a href="{{ route('logout') }}"
												onclick="event.preventDefault();
												document.getElementById('logout-form').submit();" class="badge badge-success" style="margin-top: 2px">
												Logout
											</a>
											<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
												{{ csrf_field() }}
											</form>
										</li>
									</ul>
								</li>
								@endif
							</ul>
						</div>
					</div>
				</nav>