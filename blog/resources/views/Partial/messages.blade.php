<div class="container margin-top-20">
	<div class="row">
		<div class="col-xs-8 col-sm-8 col-md-8 offset-xs-2 offset-sm-2 offset-md-2">
			@if($errors->any())
			<div class="alert alert-danger">
				<a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<ul>
					@foreach($errors->all() as $error)
					<p>{{$error}}</p>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>
</div>
<div class="container margin-top-20">
	<div class="row">
		<div class="col-xs-8 col-sm-8 col-md-8 offset-xs-2 offset-sm-2 offset-md-2">
			@if(Session::has('success'))
			<div class="alert alert-success">
				<p>
					{{Session::get('success')}}
				</p>
			</div>
			@endif
		</div>
	</div>
</div>
<div class="container margin-top-20">
	<div class="row">
		<div class="col-xs-8 col-sm-8 col-md-8 offset-xs-2 offset-sm-2 offset-md-2">
			@if(Session::has('errors'))
			<div class="alert alert-danger">
				<p>
					{{Session::get('errors')}}
				</p>
			</div>
			@endif
		</div>
	</div>
</div>