<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('title','laravel Ecommarce Project')</title>
    <link href="{{ url('')}}/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <link href="{{ url('')}}/css/bootstrap.css" rel="stylesheet" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="{{asset('css/style.css')}}" rel="stylesheet" />
</head>
<body>
	<section>
		<div class="wrapper">
			@include('Partial.header')
		</div>
	</section><!--header ends-->


	<section>
			@include('Partial.nav')

    </section> <!-- nav-bar ends -->

	@yield('content')

	<section>

		@include('Partial.footer')
	</footer> <!-- pooter ends -->
	</section>
	
    @include("Partial.scripts")
    @yield('scripts')
</body>
</html>
