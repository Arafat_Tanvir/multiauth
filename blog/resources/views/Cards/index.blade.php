@extends('Layouts.master')
@section('content')
<div class="container margin-top-20">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			@if(App\Card::total_Cards()->count() > 0)
			<table id="schedule" class="table table-bordered">
				<thead>
					<tr>
						<th rowspan="2">SL</th>
						<th rowspan="2">Product Title</th>
						<th rowspan="2">Image</th>
						<th rowspan="2">Product Quantity</th>
						<th rowspan="2">Unit Price</th>
						<th rowspan="2">Sub Total Price</th>
						<th rowspan="2">Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						@php

						$total_price=0;

						@endphp
						<div style="display: none;">{{$a=1}}</div>
						@foreach(App\Card::total_Cards() as $card)
						<td>{{ $a++ }}</td>
						<td>
							<a href="{{route('products.show',$card->product->slug)}}">{{ $card->product->title}}</a>
						</td>
						<td>
						    <div id="demo" class="carousel slide" data-ride="carousel">
								<!-- The slideshow -->
								<div class="carousel-inner">
									<div class="carousel-inner">
										@php
										$i=1
										@endphp
										@foreach($card->product->images as $image)
										<div style="text-align: center;" class="carousel-item {{$i==1 ? 'active' : ''}}">
											<img class="img-fluid img-thumbnail" src="{{asset('images/products/'.$image->images)}}" alt="" >
										</div>
										@php
										$i++
										@endphp
										@endforeach
									</div>
								</div>
								<!-- Left and right controls -->
								<a class="carousel-control-prev" href="#demo" data-slide="prev">
									<span class="carousel-control-prev-icon"></span>
								</a>
								<a class="carousel-control-next" href="#demo" data-slide="next">
									<span class="carousel-control-next-icon"></span>
								</a>
							</div>
						</td>
						<td>
							<form style="margin-left: 40px" class="form-inline" action="{{ route('cards.update', $card->id)}}" method="POST">
								{{csrf_field()}}
									<input type="number" value="{{$card->product_quantity}}" name="product_quantity" class="col-sm-6 form-control">
							        <button type="submit" class="btn btn-outline-primary btn-sm">Update</button>
							</form>
						</td>
						<td>{{$card->product->price}} Taka</td>
						<td>{{$card->product->price * $card->product_quantity}} Taka</td>
						<td>
							<a href="#DeleteModal{{ $card->id}}" data-toggle="modal" class="btn btn-outline-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$card->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('cards.destroy', $card->id)}}" method="POST">
													{{csrf_field()}}
												<button type="submit" class="btn btn-outline-primary btn-lg">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-outline-success btn-lg" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								
							</a>
						</td>
					</tr>
					@php

					$total_price+=$card->product->price * $card->product_quantity

					@endphp
					@endforeach
					<tr>
						<th style="margin-left: 120px" colspan="5">Total Amount</th>
						<th colspan="2">{{ $total_price }} Taka</th>
					</tr>
				</tbody>
			</table>
			<div class="float-right">
			<a href="{{ route('products.index')}}" class="btn btn-outline-info btn-lg">Continue Shopping</a>
			<a href="{{ route('checkout.index')}}" class="btn btn-outline-warning btn-lg">Checkout</a>
			</div>
			@else
			<p class="badge badge-success" style="text-align: center; font-size: 20px">
				No Iteam in your Card
			</p>
			<br>
			<a href="{{ route('products.index')}}" class="btn btn-outline-info btn-lg margin-top-20">Continue Shopping</a>
		    @endif
		</div>
	</div>
</div>
@endsection