<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
Auth::routes();
Route::get('/', 'HomeController@index')->name('index');

Route::get('/token/{token}','VerificationController@verify')->name('user.verification');

Route::group(['prefix'=>'admin'],function(){

//admin registration and verification
	Route::post('/register','Auth\Admin\Admin\RegisterController@register')->name('admin.register');
	Route::get('/register/submit','Auth\Admin\Admin\RegisterController@showRegistrationForm')->name('admin.register.submit');
	Route::get('/token/{token}', 'VerifyController@verify')->name('admin.verification');
//admin dashboard
	Route::get('/dashboard', 'AdminController@index')->name('admin.dashboard');
//admin login and logout
	Route::get('/login','Auth\Admin\Admin\LoginController@showLoginForm')->name('admin.login');
	Route::post('/login/submit','Auth\Admin\Admin\LoginController@login')->name('admin.login.submit');
	Route::post('/logout','Auth\Admin\Admin\LoginController@logout')->name('admin.logout');
//admin Forgotpassword and reset password
	Route::post('/password/email','Auth\Admin\Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
	Route::get('/password/reset','Auth\Admin\Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
	Route::get('/password/reset/{token}','Auth\Admin\Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
	Route::post('/password/reset','Auth\Admin\Admin\ResetPasswordController@reset')->name('admin.password.reset.post');




});

//order route details
Route::group(['prefix'=>'/orders'],function(){

	Route::get('/',"OrderController@index")->name('orders.index');
	Route::get('/create',"OrderController@create")->name('orders.create');
	Route::post('/store',"OrderController@store")->name('orders.store');
	Route::get('/show/{id}',"OrderController@show")->name('orders.show');
	Route::get('/edit/{id}',"OrderController@edit")->name('orders.edit');
	Route::post('/update/{id}',"OrderController@update")->name('orders.update');
	Route::post('/delete/{id}',"OrderController@destroy")->name('orders.destroy');

	//paid and Complete
	Route::post('/paid/{id}',"OrderController@orderPaid")->name('orders.paid');
	Route::post('/complete/{id}',"OrderController@orderComplete")->name('orders.complete');

});
Route::group(['prefix'=>'/categories'],function(){

//cotegories route details
Route::get('/',"CategoryController@index")->name('categories.index');
Route::get('/create',"CategoryController@create")->name('categories.create');
Route::post('/store',"CategoryController@store")->name('categories.store');
Route::get('/edit/{id}',"CategoryController@edit")->name('categories.edit');
Route::post('/update/{id}',"CategoryController@update")->name('categories.update');
Route::post('/delete/{id}',"CategoryController@delete")->name('categories.delete');

});







Route::get('/user/dashboard',"UserController@dashboard")->name('dashboard');
Route::get('/dashboard/profile',"UserController@profile")->name('dashboard.profile');
Route::post('/dashboard/update',"UserController@update")->name('dashboard.update');
Route::post('/dashboard/delete/{id}',"UserController@destroy")->name('user.destroy');


//products route details
Route::resource('products','ProductController');
Route::get('/product-manage','ProductController@product_manage')->name('product-manage');
Route::get('/product-search','HomeController@product_search')->name('product-search');
Route::get('/categories-show/{id}',"ProductController@categories_show")->name('categories-show');

//cotegories route details

//brands route details
Route::get('/brands',"BrandController@index")->name('brands.index');
Route::get('/brands/create',"BrandController@create")->name('brands.create');
Route::post('/brands',"BrandController@store")->name('brands.store');
Route::get('/brands/show',"BrandController@show")->name('brands.show');
Route::get('/brands/edit/{id}',"BrandController@edit")->name('brands.edit');
Route::post('/brands/update/{id}',"BrandController@update")->name('brands.update');
Route::post('/brands/delete/{id}',"BrandController@delete")->name('brands.delete');

//card
Route::get('/cards',"CardController@index")->name('cards.index');
Route::get('/cards/create',"CardController@create")->name('cards.create');
Route::post('/cards',"CardController@store")->name('cards.store');
Route::get('/cards/show',"CardController@show")->name('cards.show');
Route::get('/cards/edit/{id}',"CardController@edit")->name('cards.edit');
Route::post('/cards/update/{id}',"CardController@update")->name('cards.update');
Route::post('/cards/delete/{id}',"CardController@destroy")->name('cards.destroy');


//checkout route details

Route::get('/checkout',"CheckoutController@index")->name('checkout.index');
Route::get('/checkout/create',"CheckoutController@create")->name('checkout.create');
Route::post('/checkout',"CheckoutController@store")->name('checkout.store');
Route::get('/checkout/show',"CheckoutController@show")->name('checkout.show');
Route::get('/checkout/edit/{id}',"CheckoutController@edit")->name('checkout.edit');
Route::post('/checkout/update/{id}',"CheckoutController@update")->name('checkout.update');
Route::post('/checkout/delete/{id}',"CheckoutController@destroy")->name('checkout.destroy');

//cities
Route::get('/divisions/ajax/{id}', [
	'uses' => 'PlaceController@divisions_ajax',
	'as' => 'divisions'
]);

Route::get('/districts/ajax/{id}', [
	'uses' => 'PlaceController@districts_ajax',
	'as' => 'districts'
]);

Route::get('/upazilas/ajax/{id}', [
	'uses' => 'PlaceController@upazilas_ajax',
	'as' => 'upazilas'
]);
