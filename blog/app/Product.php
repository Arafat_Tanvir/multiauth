<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title', 'description', 'slug' ,'quantity', 'price','status','offer_price','brand_id','category_id','user_id'];
    
   public function images()
   {
   	return $this->hasMany('App\Productimage');
   }

   public function user(){
    	return $this->belongsTo('App\User');
    }

    public function brands()
    {
        return $this->belongsTo('App\Brand', 'brand_id');
    }

    public function categories(){
    	return $this->belongsTo('App\Category','category_id');
    }
}
