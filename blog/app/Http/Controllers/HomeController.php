<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id','desc')->get();
        return view('index',compact('products'));
    }

    public function product_search(Request $request)
    {
        $search=$request->search;
        //dd($search);
        $products = Product::orWhere('title','like','%'.$search.'%')
        ->orWhere('title','like','%'.$search.'%')
        ->orWhere('description','like','%'.$search.'%')
        ->orWhere('slug','like','%'.$search.'%')
        ->orWhere('price','like','%'.$search.'%')
        ->orderBy('id','desc')
        ->get();
        //dd($products);
        return view('index',compact('products','search'));
    }
}
