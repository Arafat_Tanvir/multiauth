<?php

namespace App\Http\Controllers;
use DB;
use App\District;
use App\Division;
use App\Upazila;
use App\Union;
use Illuminate\Http\Request;

class PlaceController extends Controller
{
    public function divisions_ajax($id)
    {
        $districts = DB::table('districts')->where('division_id', $id)->pluck('name','id');
        return json_encode($districts);
    }

    public function districts_ajax($id)
    {
        //dd($id);
        $upazilas=DB::table("upazilas")
        ->where("district_id",$id)
        ->pluck("name","id");
        return json_encode($upazilas);
    }

    public function upazilas_ajax($id)
    {
        $unions=DB::table("unions")
        ->where("upazila_id",$id)
        ->pluck("name","id");
        return json_encode($unions);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
