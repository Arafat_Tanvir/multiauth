<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Brand;
use DB;
use App\Productimage;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id','desc')->get();
        return view('Products.index',compact('products'));
    }

    public function product_manage()
    {
        $products = Product::orderBy('id','desc')->get();
        return view('Products.product-manage',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands=Brand::orderBy('id','desc')->get();
        return view('Products.create',compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
            'title'=>'required|max:50|min:5',
            'description'=>'required',
            'price' =>'required',

            ]
    );

        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $input['slug']=str_slug($request->title);
        $result=Product::create($input);
        $id=$result->id;
        //dd($id);

        if(count($request->images)>0){
            foreach ($request->images as $images) {
                $img=time().'.'.$images->getClientOriginalExtension();
                $location=public_path('images/products/'.$img);
                Image::make($images)->save($location);

                $product_images=new Productimage();
                $product_images->images=$img;
                $product_images->product_id=$id;
                $product_images->save();
            }
        }
        if($result){
            Session::flash('status','Product Created Successfully');
        return redirect('products');
        }else{
            Session::flash('status','Product not Created');
        return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response



     */
    public function show($slug)
    {
        $products=Product::where('slug',$slug)->first();
        //dd($products);
        if(!is_null($products))
        {
            return view('Products.show',compact('products'));
        }else{
            session()->flash('errors','Sorry !! There is no product by this URL');
            return redirect('products');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */


     public function categories_show($id)
    {
        $categories=Category::find($id);
        //dd($categories);
        if(!is_null($categories))
        {
            //dd($categories);
            return view('Categories.show',compact('categories'));
        }else{
            session()->flash('errors','Sorry !! There is no product by this URL');
            return redirect('products');
        }
    }

    public function edit($id)
    {
        $brands=Brand::orderBy('id','desc')->get();
        //dd($brands);
        $product = Product::findOrFail($id);
        return view('Products.edit',compact('product','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
            'title'=>'required|max:50|min:5',
            'description'=>'required',
            'price' =>'required',

            ]
    );

        $products=Product::findOrFail($id);
        dd($products);
        $products->title=$request->title;
        $products->description=$request->description;
        $products->slug=str_slug($request->title);
        $products->quantity=$request->quantity;
        $products->price=$request->price;
        $products->status=$request->status;
        $products->offer_price=$request->offer_price;
        $products->brand_id=$request->brand_id;
        $products->category_id=$request->category_id;
        $products->user_id=Auth::user()->id;

        if(count($request->images)>0){
            foreach ($request->images as $images) {
                $img=time().'.'.$images->getClientOriginalExtension();
                $location=public_path('images/products/'.$img);
                Image::make($images)->save($location);

                $product_images=new Productimage();
                $product_images->images=$img;
                $product_images->product_id=$id;
                $product_images->save();
            }
        }
        $products->update();
        if($products){
            Session::flash('status','Product Created Successfully');
        return redirect('products');
        }else{
            Session::flash('status','Product not Created');
        return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $products=Product::findOrFail($id);
        if(!is_null($products))
        {
            $products->delete();
        }
        session()->flash('success','Product has delete Successfully');
        return back();
    }
}
