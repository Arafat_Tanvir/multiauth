<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Brand;
use Image;
use File;

class BrandController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:web');
    }

    
    public function index()
    {
    	$brands =Brand::orderBy('id','desc')->get();
    	return view('Brands.index',compact('brands'));
    }

    public function create()
    {
    	// $brands = Brand::orderBy('name','desc')->where('parent_id',NULL)->get();
    	// //dd($brands);
        return view('Brands.create');
    }

    public function store(Request $request)
    {
    	//dd($request);
    	$this->validate($request,[
    		'name'=>'required|max:50|min:5',
    		'description'=>'required',
    	]);

    	$brands=new Brand();
    	$brands->name=$request->name;
    	$brands->description=$request->description;
        if(count($request->images)>0)
        {
        	$images=$request->file('images');
            $img=time().'.'.$images->getClientOriginalExtension();
            $location=public_path('images/brands/'.$img);
            Image::make($images)->save($location);
            $brands->images=$img;
        }
        $brands->save();

        if($brands)
        {
            Session::flash('status','brands Created Successfully');
            return redirect("brands");
        }else
        {
            Session::flash('status','brands not Created');
            return redirect()->back();
        }
    }


    public function edit($id)
    {
        //$main_brands = Brand::orderBy('name','desc')->where('parent_id',NULL)->get();
    	$brands = Brand::findOrFail($id);
        if(!is_null($brands)){
            return view('brands.edit',compact('brands'));
        }else{
            return redirect()->route('brands');
        }

        
    }


    public function update(Request $request,$id)
    {
    	//dd($request);
        $this->validate($request,[
            'name'=>'required|max:50',
            'description'=>'required',
        ]);

        $brands=Brand::findOrFail($id);
        //dd($brands);
        $brands->name=$request->name;
        $brands->description=$request->description;
        if(count($request->images)>0)
        {
            if(File::exists('images/brands/'.$brands->images))
            {
                File::delete('images/brands/'.$brands->images);
            }
            $images=$request->file('images');
            $img=time().'.'.$images->getClientOriginalExtension();
            $location=public_path('images/brands/'.$img);
            Image::make($images)->save($location);
            $brands->images=$img;
        }
        $brands->update();

        if($brands)
        {
            Session::flash('status','brands Created Successfully');
            return redirect('brands');
        }else
        {
            Session::flash('status','brands not Created');
            return redirect()->back();
        }
    }

    public function delete($id)
    {
        $brands=Brand::findOrFail($id);
        //dd($brands);
        if(!is_null($brands))
        {
            //dd($brands->parent_id);
            // if($brands->parent_id==NULL)
            // {
            //     //dd($brands->id);
            //     $sub_brands = Brand::orderBy('name','desc')->where('id',$brands->id)->get();
            //     //dd($sub_brands);
            //     foreach ($sub_brands as $sub) {
            //         if(File::exists('images/brands/'.$sub->images))
            //         {
            //             File::delete('images/brands/'.$sub->images);
            //         }
            //         //dd($sub);
            //         $sub->delete();
            //     }

            // }
            if(File::exists('images/brands/'.$brands->images)){
                File::delete('images/brands/'.$brands->images);
            }
            //dd($brands);
            $brands->delete();
        }
        session()->flash('success','Product has delete Successfully');
        return back();
    }
}
