<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
use Image;
use File;
class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index()
    {
    	$categories =Category::orderBy('id','desc')->get();
    	return view('Categories.index',compact('categories'));
    }

    public function create()
    {
    	$categories = Category::orderBy('name','desc')->where('parent_id',NULL)->get();
    	//dd($categories);
        return view('Categories.create',compact('categories'));
    }

    public function store(Request $request)
    {
    	//dd($request);
    	$this->validate($request,[
    		'name'=>'required|max:50|min:5',
    		'description'=>'required',
    	]);

    	$categories=new Category();
    	$categories->name=$request->name;
    	$categories->description=$request->description;
    	$categories->parent_id=$request->parent_id;
        if(count($request->images)>0)
        {
        	$images=$request->file('images');
            $img=time().'.'.$images->getClientOriginalExtension();
            $location=public_path('images/categories/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $categories->images=$img;
        }
        $categories->save();

        if($categories)
        {
            Session::flash('status','Categories Created Successfully');
            return redirect("categories");
        }else
        {
            Session::flash('status','Categories not Created');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $main_categories = Category::orderBy('name','desc')->where('parent_id',NULL)->get();
    	$categories = Category::findOrFail($id);
        if(!is_null($categories)){
            return view('Categories.edit',compact('categories','main_categories'));
        }else{
            return redirect()->route('categories');
        }

        
    }


    public function update(Request $request,$id)
    {
    	//dd($request);
        $this->validate($request,[
            'name'=>'required|max:50|min:5',
            'description'=>'required',
        ]);

        $categories=Category::findOrFail($id);
        //dd($categories);
        $categories->name=$request->name;
        $categories->description=$request->description;
        $categories->parent_id=$request->parent_id;
        if(count($request->images)>0)
        {
            if(File::exists('images/categories/'.$categories->images))
            {
                File::delete('images/categories/'.$categories->images);
            }
            $images=$request->file('images');
            $img=time().'.'.$images->getClientOriginalExtension();
            $location=public_path('images/categories/'.$img);
            Image::make($images)->save($location)->resize(320, 320);
            $categories->images=$img;
        }
        $categories->update();

        if($categories)
        {
            Session::flash('status','Categories Created Successfully');
            return redirect('categories');
        }else
        {
            Session::flash('status','Categories not Created');
            return redirect()->back();
        }
    }

    public function delete($id)
    {
        $categories=Category::findOrFail($id);
        //dd($categories);
        if(!is_null($categories))
        {
            //dd($categories->parent_id);
            if($categories->parent_id==NULL)
            {
                //dd($categories->id);
                $sub_categories = Category::orderBy('name','desc')->where('id',$categories->id)->get();
                //dd($sub_categories);
                foreach ($sub_categories as $sub) {
                    if(File::exists('images/categories/'.$sub->images))
                    {
                        File::delete('images/categories/'.$sub->images);
                    }
                    //dd($sub);
                    $sub->delete();
                }

            }
            if(File::exists('images/categories/'.$categories->images)){
                File::delete('images/categories/'.$categories->images);
            }
            //dd($categories);
            $categories->delete();
        }
        session()->flash('success','Product has delete Successfully');
        return back();
    }


}
