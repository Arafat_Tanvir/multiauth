<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\User;
use Image;
use File;

class UserController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $user=Auth::user();
        return view('Users.index',compact('user'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http \Response
     */


    public function profile()
    {
        $user=Auth::user();
        //dd($user);
        return view('Users.edit',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd('fasfkd');
        $user=Auth::user();

        $this->validate($request,[
            'first_name' => 'required|string|max:30',
            'last_name' => 'required|string|max:20',
            'username' => 'required|string|alpha_dash|max:100|unique:users,username,'.$user->id,
            'email' => 'required|string|email|max:100|unique:users,email,'.$user->id,
            'phone' => 'required|numeric|unique:users,phone,'.$user->id,
            'shipping_address' => 'required|string|max:200',
            'division_id' => 'required|numeric',
            'district_id' => 'required|numeric',
            'upazila_id' => 'required|numeric',
            'union_id' => 'required|numeric',
            'street_address' => 'required|string|max:100',

        ]);

        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->username=$request->username;
        $user->phone=$request->phone;
        $user->ip_address=request()->ip();
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
        $user->shipping_address=$request->shipping_address;
        $user->division_id=$request->division_id;
        $user->district_id=$request->district_id;
        $user->upazila_id=$request->upazila_id;
        $user->union_id=$request->union_id;
        $user->street_address=$request->street_address;
        //dd($user);
        if(count($request->images)>0)
        {
            if(File::exists('images/users/'.$user->images))
            {
                File::delete('images/users/'.$user->images);
            }
            $images=$request->file('images');
            $img=time().'.'.$images->getClientOriginalExtension();
            $location=public_path('images/users/'.$img);
            Image::make($images)->save($location)->resize(300, 200)->save('foo.jpg');
            $user->images=$img;
        }
        $user->update();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
