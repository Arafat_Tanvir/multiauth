<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Card;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use File;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Cards.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $this->validate($request,[
            'product_id'=>'required',
        ]);

        if(Auth::check()){
            $cards=Card::where('user_id',Auth::id())
            ->where('product_id',$request->product_id)
            ->where('order_id',NULL)
            ->first();

        }else
        {
            $cards=Card::where('ip_address',request()->ip())
            ->where('product_id',$request->product_id)
            ->where('order_id',NULL)
            ->first();
            //dd($cards);
        }
        if (!is_null($cards)){

            $cards->increment('product_quantity');

        }else{

            $cards=new Card();
            if(Auth::check()){
                $cards->user_id=Auth::id();
            }
            $cards->product_id=$request->product_id;
            $cards->ip_address=$request->ip();
            $cards->save();

            session::flash('success','cards Created Successfully');
            return back();
        }
            
    
            session::flash('strictly_error','cards not Created');
            return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cards=Card::findOrFail($id);
        if(!is_null($cards)){
            $cards->product_quantity=$request->product_quantity;
            $cards->update();
            return redirect('products');
        }else{
            return redirect('cards');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cards=Card::findOrFail($id);
        //dd($cards);
        if(!is_null($cards))
        {
            $cards->delete();
            
        }else{
            return redirect('cards');
        }
        session()->flash('success','Product has delete Successfully');
        return back();
    }
}
