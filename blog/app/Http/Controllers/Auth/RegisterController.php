<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Image;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\VerifyRegistration;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'images' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(Request $request)
    {
        //dd($request);
        if(count($request->images)>0)
        {
            //dd($request->images);
            $images=$request->file('images');
            $img=time().'.'.$images->getClientOriginalExtension();
            $location=public_path('images/users/'.$img);
            Image::make($images)->save($location)->resize(300, 200)->save('foo.jpg');
        }

        //dd($img);

        $user= User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'username' => str_slug($request->first_name.$request->last_name),
            'phone' => $request->phone,
            'images'=> $img,
            'ip_address' => request()->ip(),
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'shipping_address' => $request->shipping_address,
            'division_id' => $request->division_id,
            'district_id' => $request->district_id,
            'upazila_id' => $request->upazila_id,
            'union_id' => $request->union_id,
            'street_address' => $request->street_address,
            'remember_token'=>str_random(50),
            'status'=>0,
        ]);


        $user->notify(new VerifyRegistration($user,$user->remember_token));
        session()->flash('success','A confirmation email has sent to you,please check your email and click to registration confirmed');
        return redirect('/');
    }
}
