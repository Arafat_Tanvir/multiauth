<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Notifications\VerifyRegistration;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        //find user by this email
        $user=User::where('email',$request->email)->first();
        if (!$user==NULL) {

            if($user->status==1)
            {
                # code...
                if (Auth::guard('web')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember))
                {
                    # login now
                    return redirect()->intended(route('index'));
                }else{
                    session()->flash('success','Your email and password is wrong! try again');
                    return redirect('login');
                }
            }else
            {
                $user->notify(new VerifyRegistration($user,$user->remember_token));
                session()->flash('success','A new confirmation email has sent to you,please check your email and click to registration confirmed');
                return redirect('/');
            }
        }else{
            session()->flash('success','You have no account!please Registration');
            return redirect('/');
        }
    }
}
