<?php

namespace App\Http\Controllers\Auth\Admin\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\AdminVerifyRegistration;
use App\Admin;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.admin.login');
    }

    public function login(Request $request)
    {
        //dd('jfskldf');
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required|string',
        ]);
        $admin=Admin::where('email',$request->email)->first();
        //dd($admin);
        if (!is_null($admin)) {
            if($admin->status==1){
                if(Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember)){
                    return redirect()->intended(route('admin.dashboard'));
                    }else{
                        session()->flash('success','Invaild your password and email!');
                        return redirect()->route('admin.login');
                    }

                }else{
                    $admin->notify(new AdminVerifyRegistration($admin->remember_token));
                    session()->flash('success','A  New confirmation email has be sent to you,please check and confirmed your registration');
                    return redirect()->route('admin.login');
            }
        }else{
            session()->flash('sticky_error','please registration');
                return redirect()->route('admin.login');
        }


    }
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect()->route('admin.login');
    }
}
