<?php
namespace App\Helper;

 class GravatarHelper
 {
 	public static function validate_gravatar($email)
 	{
 		// dd($email);
 		$hash=md5($email);
 		//dd($hash);
 		$url='https://s.gravatar.com/avatar/'.$hash.'?s=400';

 		//dd($url);
 		
 		$headers=get_headers($url);
 		//dd($headers);

 		if(!preg_match("|200|", $headers[0])){
 			$has_valid_avatar=FALSE;
 		}else{
 			$has_valid_avatar=TRUE;
 		}
 		//dd($has_valid_avatar);
 		return $has_valid_avatar;
 	}

 	public static function gravatar_image($email,$size=0,$d=""){
 		//dd($email);
 		$hash=md5($email);
 		$image_url='https://s.gravatar.com/avatar/'.$hash.'?s='.$size.'&d='.$d;
 		return $image_url;
 	}


 }

 ?>