<?php
namespace App\Helper;

use App\User;
use App\Helper\GravatarHelper;


/**
 * 
 */
class ImageHelper
{
	public static function getUserImage($id)
	{
		//dd($id);
		$user=User::find($id);
		//dd($user->email);
        $avatar_url="";
        if(!is_null($user))
        {
	        if($user->avatar==NULL)
	        {
		        if(GravatarHelper::validate_gravatar($user->email))
		        {
			        $avatar_url=GravatarHelper::gravatar_image($user->email,10000);
		        }else{
		        	$avatar_url=url('images/users/default.png');
		        }
	        }else{
	        	$avatar_url=url('images/users/'.$user->images);
	        }

        }else{

        }
        return $avatar_url;
    }
	
}

?>