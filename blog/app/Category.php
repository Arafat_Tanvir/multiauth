<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $fillable = ['name','description','images','parent_id'];
    public function parent()
    {
    	return $this->belongsTo(Category::class,'parent_id');
    }
    public function products()
	{
	   return $this->hasMany(Product::class);
	}


	/**
     * Store a newly created resource in storage.
     *
     * @param  int $parent_id
     * @param int $child_id
     */
	public static function ParentOrNotCategory($parent_id,$child_id)
	{
		$categories=Category::where('id',$child_id)
		->where('parent_id',$parent_id)
		->get();
		if(!is_null($categories))
		{
			return true;
		}else
		{
			return false;
		}
	}
}
